document.addEventListener("DOMContentLoaded", function () {
    const breedsList = document.getElementById("breeds-list");
    const breedSelect = document.getElementById("breed-select");
    const confirmButton = document.getElementById("confirm-button");

    function showBreedImage(selectedBreed) {
        breedsList.innerHTML = "";

        axios.get(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
            .then(response => {
                const breedItem = document.createElement("li");
                breedItem.classList.add("breed-item");

                const img = document.createElement("img");
                img.src = response.data.message;
                img.alt = selectedBreed;
                breedItem.appendChild(img);

                const breedName = document.createElement("p");
                breedName.classList.add("breed-name");
                breedName.textContent = selectedBreed;
                breedItem.appendChild(breedName);

                breedsList.appendChild(breedItem);
                breedItem.style.display = "block"; 
            })
            .catch(error => console.error(`Error al obtener imagen para ${selectedBreed}:`, error));
    }

    axios.get("https://dog.ceo/api/breeds/list")
        .then(response => {
            const breeds = response.data.message;

            breeds.forEach(breed => {
                const option = document.createElement("option");
                option.value = breed;
                option.textContent = breed;
                breedSelect.appendChild(option);
            });

            confirmButton.addEventListener("click", function () {
                const selectedBreed = breedSelect.value;
                showBreedImage(selectedBreed);
            });
        })
        .catch(error => console.error("Error al obtener la lista de razas de perros:", error));
});